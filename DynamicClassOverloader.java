import java.io.*;

public class DynamicClassOverloader extends ClassLoader
{
    private java.util.Map classesHash= new java.util.HashMap();
    public final String classPath;
  //  private static final String extension = ".class";

    public DynamicClassOverloader(String classPath)
    {
        this.classPath= classPath;
    }

    protected synchronized Class loadClass(String name, boolean resolve) throws ClassNotFoundException
    {
        Class result= findClass(name);
        if (resolve)
            resolveClass(result);
        return result;
    }

    protected Class findClass(String name) throws ClassNotFoundException
    {
        Class result= (Class)classesHash.get(name);
        if (result!=null) {
            return result;
        }

        File f= findFile(name.replace('.','/'), ".class");

        if (f==null) {
            return findSystemClass(name);
        }

        try {
            byte[] classBytes= loadFileAsBytes(f);
            result = defineClass(name,classBytes,0,classBytes.length);
        } catch (IOException e) {
            throw new ClassNotFoundException("err: cannot load class "+name+": "+e);
        } catch (ClassFormatError e) {
            throw new ClassNotFoundException("err: format of class file incorrect for class "+name+": "+e);
        }

        classesHash.put(name,result);
        return result;
    }

    protected java.net.URL findResource(String name)
    {
       return null;
    }

    private File findFile(String name, String extension)
    {
        File f= new File((new File(classPath)).getPath() +
                File.separatorChar + name.replace('/',File.separatorChar) + extension);
        if (f.exists())
            return f;
        return null;
    }

    private static byte[] loadFileAsBytes(File file) throws IOException
    {
        byte[] result = new byte[(int)file.length()];
        FileInputStream f = new FileInputStream(file);
        try {
            f.read(result,0,result.length);
        }
        catch (Exception e){
            System.out.println("err: cannot read file: " + e);
        }
        finally {
            f.close();
        }
        return result;
    }
}