//package inet_protocols;

import java.io.BufferedWriter;
import interfaces.*;

public class secondProtocol implements CommandProcessor, CPFactory{
    @Override
    public int processCommand(String read, BufferedWriter writer) {
        switch (read){
            case "a":
                return TryWriteToBuffer("aaaaaa", writer);
            case "s":
                return TryWriteToBuffer("ssssss", writer);
            case "d":
                return TryWriteToBuffer("dddddd", writer);
            case "f":
                return TryWriteToBuffer("ffffff", writer);
            case "g":
                return TryWriteToBuffer("gggggg", writer);
            case "h":
                return TryWriteToBuffer("hhhhhh", writer);
            default:
                return TryWriteToBuffer("used unknown command", writer);
        }
    }

    @Override
    public CommandProcessor getNewProcessor() {
        return this;
    }

    private int TryWriteToBuffer(String command, BufferedWriter writer){
        try {
            writer.write(command + '\n');
        }
        catch (Exception e){
            System.out.println("err: cannot write to buffer " + e);
            return 1;
        }
        return 0;
    }
}
