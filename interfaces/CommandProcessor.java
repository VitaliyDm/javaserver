package interfaces;

import java.io.BufferedWriter;

public interface CommandProcessor {
    public abstract int processCommand(String read, BufferedWriter writer);
}
