import interfaces.*;

import java.net.*;
import java.io.*;


public class serverUni {
    public static CPFactory factory;
    public static final String pathToProtocols = "./server/inet_protocols";
    public static void main(String[] args){
        ServerWorker worker;
        Socket socket;
        ServerControl controler;
        DynamicClassOverloader overloader = new DynamicClassOverloader(pathToProtocols);

        //Initialize start standard protocol
        try {
            Class clazz = Class.forName("firstProtocol", true, overloader);
            factory = (CPFactory)clazz.newInstance();
        }
        catch (Exception e){
            System.out.println("err: cannot open file with class");
        }


        try{
            ServerSocket commandSocket = new ServerSocket(1001);
            ServerSocket serverSocket = new ServerSocket(101);
            System.out.println("server started");
            CommandSocketListiner commandListiner = new CommandSocketListiner(commandSocket, overloader);
            commandListiner.start();

            while (true){
               // if (serverSocket.)
                socket = serverSocket.accept();
                worker = new ServerWorker(socket, factory);
                worker.start();
            }
        }
        catch (Exception e){
            System.out.println("err: server init error" + e);
        }
    }

    private static class CommandSocketListiner extends Thread{
         private ServerSocket commandSocket;
         private DynamicClassOverloader loader;
         CommandSocketListiner(ServerSocket socket, DynamicClassOverloader loader){
             this.commandSocket = socket;
             this.loader = loader;
         }

         @Override
        public void run(){
             Socket socket;
             ServerControl controler;
             try {
                while (true){
                   socket = this.commandSocket.accept();
                   controler = new ServerControl(socket, pathToProtocols, loader);
                   controler.start();
                }
             }
             catch (Exception e){
                 System.out.println("err: comand listener error " + e);
             }
         }
    }
}