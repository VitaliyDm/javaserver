import interfaces.CPFactory;
import interfaces.CommandProcessor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class ServerWorker extends Thread{

    private Socket socket;
    private CPFactory factory;

    ServerWorker(Socket userSocket, CPFactory userFactory){
        this.socket = userSocket;
        this.factory = userFactory;
    }

    @Override
    public void run() {
        String line;
        int result;
        BufferedReader rb;
        BufferedWriter wb;

        CommandProcessor commandProc = factory.getNewProcessor();
        System.out.println(socket.getRemoteSocketAddress().toString() + " user connected");

        try{
            rb = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            wb = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            while (!socket.isClosed()){
                line = rb.readLine();
                result = commandProc.processCommand(line, wb);
                if (result != 0){
                    System.out.println("err: error in protocol");
                    break;
                }
                wb.flush();
            }
        }
        catch (Exception e){
            System.out.println("err: " + e);
        }
    }
}
