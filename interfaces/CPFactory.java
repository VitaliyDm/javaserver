package interfaces;

public interface CPFactory {
    public abstract CommandProcessor getNewProcessor();
}

