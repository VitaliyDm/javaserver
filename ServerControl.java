import interfaces.CPFactory;

import java.io.*;
import java.net.CacheRequest;
import java.net.Socket;

public class ServerControl extends Thread {

    private Socket socket;
    private String pathToProtocols;
    private DynamicClassOverloader loader;

    ServerControl(Socket socket, String pathToProtocols, DynamicClassOverloader loader){
        this.socket = socket;
        this.pathToProtocols = pathToProtocols;
        this.loader = loader;
    }

    @Override
    public void run(){
        try{
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            DataOutputStream output = new DataOutputStream(outputStream);

            String adr = socket.getRemoteSocketAddress().toString();
            System.out.println("User " + adr + " connected as admin");

            while (!socket.isClosed()){
                StringBuilder requestAns = new StringBuilder();
                    String[] inputString = reader.readLine().split("\\s");

                switch (inputString[0]){
                    case "list":
                        try{
                            for (String protoocol: new File(pathToProtocols).list()) {
                                if (protoocol.matches(".*.class"))
                                    requestAns.append(protoocol.split(".class")[0] + "\n");
                            }
                        }
                        catch (Exception e){
                            requestAns.append("err: cannot find protocols " + e);
                        }
                        break;
                    case "set":
                        try {
                            Class clazz = Class.forName(inputString[1], true, loader);
                            serverUni.factory = (CPFactory)clazz.newInstance();
                        }
                        catch (Exception e){
                            requestAns.append("err: cannot open file with class");
                            System.out.println("err: cannot open file with class");
                            break;
                        }
                        requestAns.append("Loaded: " + inputString[1]);
                        break;
                    case "exit":
                        socket.close();
                        requestAns.append("Disconnecting");
                }

                output.write(requestAns.toString().getBytes());
                output.flush();
            }
        }
        catch (Exception e){
            System.out.println("err: Connection error as admin " + e);
        }
    }

}
