//package inet_protocols;

import interfaces.*;
import java.io.BufferedWriter;

public class firstProtocol implements CommandProcessor, CPFactory {
    @Override
    public int processCommand(String read, BufferedWriter writer) {
        switch (read){
            case "q":
                return TryWriteToBuffer("qqqqqq", writer);
            case "w":
                return TryWriteToBuffer("wwwwww", writer);
            case "e":
                return TryWriteToBuffer("eeeeee", writer);
            case "r":
                return TryWriteToBuffer("rrrrrr", writer);
            case "t":
                return TryWriteToBuffer("ttttttt", writer);
            case "y":
                return TryWriteToBuffer("yyyyyyy", writer);
            default:
                return TryWriteToBuffer("used unknown command", writer);
        }
    }

    @Override
    public CommandProcessor getNewProcessor() {
        return this;
    }

    private int TryWriteToBuffer(String command, BufferedWriter writer){
        try {
            writer.write(command + '\n');
        }
        catch (Exception e){
            System.out.println("err: cannot write to buffer " + e);
            return 1;
        }
        return 0;
    }
}
